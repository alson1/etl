from unittest import TestCase
from helpers import log, log_ln, requests_json,get_int,get_str,get_json,get_datetime
from datetime import datetime
datetime(2002, 12, 25,1,1,1)

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from games import Games
from config import settings
from helpers import log_ln, log
import hashlib

class TestHelpers(TestCase):
    """
    Unit Tests
    """

    def test_get_int(self):
        self.assertEqual(get_int("string"),None)
        self.assertEqual(get_int(5),5)
        self.assertEqual(get_int({'v':4}),None)

    def test_get_str(self):
        self.assertNotEqual(get_str("string"),None)
        self.assertEqual(get_str("string"),"string")
        self.assertNotEqual(get_str(5),5)
        self.assertNotEqual(get_str({'v':4}),None)

    def test_get_json(self):
        self.assertEqual(get_json("string"),None)
        self.assertNotEqual(get_json("string"),"string")
        self.assertNotEqual(get_json(5),5)
        self.assertEqual(get_json({'v':4}),{'v':4})

    def test_get_datetime(self):
        self.assertEqual(get_datetime("2219-07-22 02:09:05"), datetime(2219, 7, 22,2,9,5))
        self.assertNotEqual(get_datetime("2219-07-22 02:09:05.1561"), datetime(2219, 7, 22,2,9,5))
        self.assertEqual(get_datetime("2219-02-30 02:09:05"), None)

        self.assertEqual(get_datetime("string"), None)
        self.assertNotEqual(get_datetime("string"), "string")
        self.assertNotEqual(get_datetime(5), 5)
        self.assertNotEqual(get_datetime({'v': 4}), {'v': 4})
        self.assertEqual(get_datetime({'v': 4}), None)

