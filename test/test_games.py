from unittest import TestCase
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from games import Games
from config import settings
from helpers import log_ln, log
import hashlib

class TestGames(TestCase):
    """
    For testing the ETL pipeline we use cached requests and a reference database
    we build the new database with the cached requests and we compare the two database with :
        - get_equivalent_api_number_of_row_database()
        - full text comparaison (database needed to be sorted)

    we test the 3 majors process of our ETL

        - First init of the database
        - the incremental udptate strategy
        - the reset database strategy

    """

    def setUp(self):
        settings.init_test()
        self.reference_games = Games(create_engine('sqlite:///test/data/database_reference/games.db', echo=False),
                                "NOT_NEEDED_APIKEY_FOR_TEST", batch_size=100)
        reference_Session = sessionmaker(bind=self.reference_games.engine)
        reference_Session.configure(bind=self.reference_games.engine)
        self.reference_session = reference_Session()

    def records_to_hash(self,records,columns):
        data = ""
        for curr in records:
            for column in columns:
                data += str(getattr(curr, column.name))
        return hashlib.md5(data.encode("utf")).hexdigest()

    def get_hash_reference_database(self):
        records = self.reference_session.query(Games).order_by(Games.id_plateform).all()
        return self.records_to_hash(records, self.reference_games.__mapper__.columns)


    def test_init_database(self):
        ''' Test of first init database'''
        #in memory database
        test_games = Games(create_engine('sqlite://', echo=False), "NOT_NEEDED_APIKEY_FOR_TEST", batch_size=100)
        Session = sessionmaker(bind=test_games.engine)
        Session.configure(bind=test_games.engine)
        session = Session()
        test_games.reset_table()
        test_games.init_load(session)

        # "count" check
        self.assertEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),test_games.get_equivalent_api_number_of_row_database(session))

        # full text check
        records = session.query(Games).order_by(Games.id_plateform).all()
        self.assertEqual(self.get_hash_reference_database(),self.records_to_hash(records, test_games.__mapper__.columns))



    def test_incremental_udptate_strategy(self):
        # in memory database
        test_games = Games(create_engine('sqlite://', echo=False), "NOT_NEEDED_APIKEY_FOR_TEST", batch_size=100)
        Session = sessionmaker(bind=test_games.engine)
        Session.configure(bind=test_games.engine)
        session = Session()
        test_games.reset_table()
        test_games.init_load(session)

        # "count" check
        self.assertEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),
                         test_games.get_equivalent_api_number_of_row_database(session))

        # even in  full text check
        records = session.query(Games).order_by(Games.id_plateform).all()
        self.assertEqual(self.get_hash_reference_database(),
                         self.records_to_hash(records, test_games.__mapper__.columns))


        # delete the last 50 updated records in the database
        session.execute(
            '''Delete from games where id IN
            (
              select id from games order by date_last_updated desc limit 50
            );''')
        session.commit()

        # check we are no longer synchronized
        self.assertNotEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),test_games.get_equivalent_api_number_of_row_database(session))

        # incremental update
        test_games.incremental_udptate_strategy(session)

        # check we are now syncronized with the reference database
        self.assertEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),test_games.get_equivalent_api_number_of_row_database(session))

        # even in  full text check
        records = session.query(Games).order_by(Games.id_plateform).all()
        self.assertEqual(self.get_hash_reference_database(),self.records_to_hash(records, test_games.__mapper__.columns))

    def test_reset_database_strategy(self):
        # in memory database
        test_games = Games(create_engine('sqlite://', echo=False), "NOT_NEEDED_APIKEY_FOR_TEST", batch_size=100)
        Session = sessionmaker(bind=test_games.engine)
        Session.configure(bind=test_games.engine)
        session = Session()
        test_games.reset_table()
        test_games.init_load(session)

        # "count" check
        self.assertEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),
                         test_games.get_equivalent_api_number_of_row_database(session))
        # even in full text
        # full text check
        records = session.query(Games).order_by(Games.id_plateform).all()
        self.assertEqual(self.get_hash_reference_database(),
                         self.records_to_hash(records, test_games.__mapper__.columns))


        # delete the last 50 first added records in the database
        session.execute(
            '''Delete from games where id IN
            (
              select id from games order by date_added asc limit 50
            );''')
        session.commit()

        # check we are no longer synchronized
        self.assertNotEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),test_games.get_equivalent_api_number_of_row_database(session))

        # we try incremental update
        test_games.incremental_udptate_strategy(session)

        # incremental update not working for this kind of synchronisation issue
        # we check we still no longer synchronized
        self.assertNotEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),test_games.get_equivalent_api_number_of_row_database(session))

        # Now we test reset_database_strategy to synchronize
        test_games.reset_database_strategy(session)
        # check we are now syncronized with the reference database
        self.assertEqual(self.reference_games.get_equivalent_api_number_of_row_database(self.reference_session),test_games.get_equivalent_api_number_of_row_database(session))

        # even in  full text check
        records = session.query(Games).order_by(Games.id_plateform).all()
        self.assertEqual(self.get_hash_reference_database(),self.records_to_hash(records, test_games.__mapper__.columns))
