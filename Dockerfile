# Dockerfile
FROM  python:3.7.4
MAINTAINER alson

RUN apt-get update
RUN apt-get install -y sudo python-pip build-essential libssl-dev libffi-dev python-dev vim wget curl nano cron
RUN pip install --upgrade pip
RUN pip install requests sqlalchemy python-dateutil
RUN groupadd -g 999 appuser && \
    useradd -m -r -u 999 -g appuser appuser 

#**** SECURITY POINT INFORMATION ********
# we need root rights to launch the cron job
# but we don't want our running app have the root rights inside the container
# then we tempory add root rights to the appusser just to lauch the cron job
# AND we remove root rights (in docker-entrypoint.sh) just after the cron job was lauched
RUN usermod -a -G sudo appuser
# allow sudo without password
RUN sed -i 's/sudo\tALL=(ALL:ALL) ALL/sudo\tALL=(ALL:ALL) NOPASSWD: ALL/g' /etc/sudoers
#**** END SECURITY POINT INFORMATION ********

WORKDIR "/home/appuser"
COPY docker/docker-entrypoint.sh /home/appuser
COPY docker/docker-cmd-start.sh /home/appuser
COPY *.py /home/appuser/

RUN mkdir config
COPY config/*.py  /home/appuser/config/

ADD test /home/appuser/test/

RUN chown -R appuser:appuser /home/appuser
USER appuser
RUN chmod +x /home/appuser/docker-entrypoint.sh
RUN chmod +x /home/appuser/docker-cmd-start.sh

ENTRYPOINT ["/home/appuser/docker-entrypoint.sh"]

CMD ["./docker-cmd-start.sh"]

# docker build . -t etl_giant_bomb
# docker run  -e cron_tab='* * * * *' -e api_key=dfgdfg -e batch_size=100 etl_giant_bomb python etl_giant_bomb.py

