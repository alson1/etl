#!/bin/bash

set -e

if [ -z ${cron_tab+x} ];
then
    echo "-------------- NO CRON --------------------";
else
    echo "CRON is set to '$cron_tab'";
    # add quotes to value
    printenv | sed 's/\(=[[:blank:]]*\)\(.*\)/\1"\2"/'  | sed 's/^\(.*\)$/export \1/g' > /home/appuser/project_env.sh
    touch /home/appuser/crontab.tmp
    sudo chmod +x /home/appuser/project_env.sh
    sudo echo "$cron_tab . /home/appuser/project_env.sh; /usr/local/bin/python /home/appuser/etl_giant_bomb.py >> /home/appuser/db/db.log  2>&1" > /home/appuser/crontab.tmp
    sudo crontab -u appuser /home/appuser/crontab.tmp
    sudo rm -rf /home/appuser/crontab.tmp
    #launch du cron
    sudo /usr/sbin/cron
 fi

#**** SECURITY POINT INFORMATION ********
# we suppress the possibility to sudo without password
sudo sed -i 's/sudo\tALL=(ALL:ALL) NOPASSWD: ALL/sudo\tALL=(ALL:ALL) ALL/g' /etc/sudoers
# now appuser can't sudo, and there is no possibilty for appuser to get root rights now
# this container is now running without sudo rights, just the cron job was launch with root rights
#**** END SECURITY POINT INFORMATION ********

exec "$@"


