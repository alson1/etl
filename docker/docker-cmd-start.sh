#!/usr/bin/env bash

if [ -z ${cron_tab+x} ];
then
    /usr/local/bin/python /home/appuser/etl_giant_bomb.py
else
    touch /home/appuser/db/db.log
    tail -f /home/appuser/db/db.log
fi