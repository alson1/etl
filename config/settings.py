from config import config
from helpers import log_ln

'''
Change global config environment variables
'''
def default():
    config.url_base_games = "https://www.giantbomb.com/api/games/"
    # SNES=>9, NES=>21, N64=>43
    config.plateformes_games = ['9', '21', '43']
    # timeout for request
    config.timeout = 60
    # sleeping time after request error
    config.sleeping = 30;
    config.cache = False
    config.cache_dir = "cache"

def init_dev():
    default()
    config.cache = False
    config.timeout = 30
    config.sleeping = 3;
    log_ln("Developpement environment")


def init_prod():
    default()
    config.cache = False
    log_ln("Production environment")


def init_test():
    init_prod()
    config.cache = True
    config.cache_dir = "test/data/cached_request"
    log_ln("Test environment")
