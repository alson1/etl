
Take Home Test
------
Design and implement a reusable ETL process to move the data from the API to be accessible via SQLite (to keep it simple). Following are the requirements:
- the database should stay in sync with the data the API can provide;
- the process should be generic enough to be extended to other APIs.

Make the most relevant assumptions to comply with the requirements if needed. 

You should use the Giant Bomb video game API as your data source for listings: http://www.giantbomb.com/api/documentation#toc-0-15

<b>Deliverables:</b>
- A SQLite database that includes all games from NES, SNES and N64.
- An ETL tool that can move the data from the API to the database.
- Any related files necessary to run the code. 

We want to be able to search for NES, SNES, and N64 games by title. You can use the offset and platforms parameters to fetch games in batches.

## Deliverables


1/ The SQLite database giantbomb API <b>db/games.db</b> (can also be rebuilt in the first execution).


2/ A lightweight simple python framework to help building a dockerized ETL Python application with scheduled functions.

Based on :
- SQLAlchemy ORM to define the data model and the database connection.
- Simple cache http-requests mechanism used to :
    - speed up the development (giantbomb api is very slow)
    - generate data for automated tests
- Example of code and automated test for the giantbomb API
- Docker container with CRON Scheduled executions
- A continuous integration pipeline with GitLab CI/CD in the cloud
[https://gitlab.com/alson1/etl/pipelines](https://gitlab.com/alson1/etl/pipelines)
 ![](./images/pipeline3.png)
- Stage BUILD
    - Build the docker image container
- Stage TEST
    - Launch auto-tests with the dockerized app
- Stage PUBLISH
   - If auto-tests are OK
     - Publish the docker image on Gitlab registry.gitlab.com [https://gitlab.com/alson1/etl/container_registry](https://gitlab.com/alson1/etl/container_registry)
## Security
- The API_KEY is never present in the code

## QuickSart
### Launch the auto-test
No API_KEY needed.<br>
`docker run registry.gitlab.com/alson1/etl:latest python -m unittest discover`<br>

### One time execution
For the first init database (for example)

`docker run -e batch_size=100 -e api_key=XXXXX -v /local_FULL_path_for_database:/home/appuser/db registry.gitlab.com/alson1/etl:latest`<br>

### CRON Scheduled execution
For incremental updates and keep sync.

`docker run -e cron_tab='*/5 * * * *' -e batch_size=100 -e api_key=XXXXX -v /local_FULL_path_for_database:/home/appuser/db registry.gitlab.com/alson1/etl:latest`<br>
<br>  Logs in db/db.log<br>

When you use cron, you have to define a long enough time interval to be sure we don't have two executions at the same time.
To avoid this case when we have of network problem, we try 3 times a new connection after the timeout then we stop the program if the problem is not fixed.
The new execution will be launched by the "supervisor" CRON. That's why <b>Apache Airflow</b> will replace cron in the application road map.

## Parameters
### Application
- <b>-e api_key=xxxxxx</b>, your api_key from www.giantbomb.com
- <b>-e batch_size=100</b>, size of batch to fetch games, <b>max=100</b> (API limitation)
- <b>-e cron_tab='*/5 * * * *'</b>, same syntax as cron job. '*/5 * * * *' => every 5 minutes
### Docker
- <b>-v /local_FULL_path_for_database:/home/appuser/db</b>, change local_FULL_path_for_database to the full pass where the database is stored

## With Docker Compose...

Download 'docker-compose.yml'

Update de the <b>api_key and volume</b> in 'docker-compose.yml'

Start the service: `docker-compose --project-name etl_giant up -d`<br>
Stop the service: `docker-compose --project-name etl_giant down`<br>

## Incremental update strategy
- We check with the <b>date_last_updated</b> field if we need an incremental update.

## Reset database strategy
- If we loose the coherence between the API and the database we reset the database.

For example when :
- the "date_last_updated" in database > "date_last_updated" in the API
- the "row count in database > row count in API"
- the "date_last_updated" in data base  = "date_last_updated" in the API <b>and</b> "row count in database <b><</b> row count in API"

## Generic assumptions on that API (to be extended to other APIs)

- <b>date_last_updated</b> field can be use to find new records or new updated records
- we can use an <b>offset</b>
- we can <b>filter</b> row by date_last_updated interval
- we can <b>sort</b> rows by date_last_updated
- we can <b>limit</b> the request

We need a <b>quick request to find the total row number of the database with the API</b> to check easily the integrity of the number of row in the database and in the api.
In giantbomb api, each request returns the number of total results. With limit=0 we have quickly this result with no extra data.

## How we can reuse this ETL process to another API

This code is not 100% generic, but we can easily adapt to a new API rules.

1. Create a new class new_api.py based on games.py

2. Define a new table_name and data schema

3. Adapt to the new class to the new API rules (init, extract, transform,...)


## Code architecture
    - etl_giant_bomb.py => app main
    - games.py => Data Model and core of ETL
    - helpers.py => some small functions to help
    - /config
        - config.py
        - settings.py => global variables by environnement, dev, prod, test
    - Dockerfile => Used to build the docker image with all dependencies
    - docker-compose.yml
    - /docker
        (2 files used to build the container)
        - docker-cmd-start.sh
        - docker-entrypoint.sh
    - /test
        - test_games.py => test the ETL Pipeline and sync
        - test_helpers.py => helpers tests
        - /data/cached_request
            - xxx.pickle
            - ....
             cached request used for test
        - /data/database_reference
            - games.db => database to compare with test database
    - .gitlab-ci.yml
        - Continuous integration pipeline
            - Stage
                - build
                - test
                - publish

## Road map
    - Use parallelism if necesseray
    - Replace CRON by Apache Airflow
    - Add better strategies in case of network problem (retry,...)
    - Add auto-tests for:
        - networks
        - docker commande line parameters

