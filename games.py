from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, JSON, ARRAY
from sqlalchemy import exc
from sqlalchemy.sql import func
from dateutil.parser import parse
from helpers import log, log_ln, requests_json, get_int, get_str, get_json, get_datetime
from config import config

Base = declarative_base()
class Games(Base):
    """
       SQLAlchemy Class to implement ETL for synchronizing https://www.giantbomb.com/api/games/ API to SQLite database
       with SQLAlchemy ORM we can  easly :
         - define/change the data model to other need/api
         - define/change the database output
    """
    __tablename__ = 'games'
    # ------------------------------------------------------------------------------
    # DATA SCHEMA
    # ------------------------------------------------------------------------------
    # we don't use id as primary_key but a new colomn __id_plateform__ build in the extract steep
    # because we want be able to bulk de data initial load
    # id = Column(Integer, primary_key=True)
    title = Column(String)
    id = Column(Integer)
    id_plateform = Column(String, primary_key=True)
    # DATETIME
    date_added = Column(DateTime)
    original_release_date = Column(DateTime)
    date_last_updated = Column(DateTime)

    # STRINGS
    aliases = Column(String)
    api_detail_url = Column(String)
    deck = Column(String)
    description = Column(String)
    guid = Column(String)
    name = Column(String)

    site_detail_url = Column(String)
    # INTEGER
    number_of_user_reviews = Column(Integer)
    expected_release_day = Column(Integer)
    expected_release_month = Column(Integer)
    expected_release_quarter = Column(Integer)
    expected_release_year = Column(Integer)
    # JSON
    image = Column(JSON)
    # ARRAY OF JSON
    # image tag are an array of JSON but sqlite doesn't support  (PostgreSQL does)
    # image_tags = Column(ARRAY(JSON))
    # Then in the TRANSFORM step we convert the array in JSON
    # [ json1, json2, json3 ...] => { "image_tags": [ json1, json2, json3 ...] }
    image_tags = Column(JSON)
    original_game_rating = Column(JSON)
    platforms = Column(JSON)
    # computated field to keep in memory (after deleting duplicate) how lines came from each platforme
    __count_platforms__ = Column(Integer)
    # log of rejected field
    # futur use
    __rejected_fields_log__ = Column(String)
    # -------------------------- END DATA SCHEMA --------------------------

    def __init__(self, engine, api_key, batch_size):
        self.engine = engine
        self.api_key = api_key
        self.batch_size = batch_size
        self.url_base = config.url_base_games
        self.plateforms = config.plateformes_games
    # ------------------------------------------------------------------------------
    # EXTRACT
    # ------------------------------------------------------------------------------
    def etl_extract(self, offset, limit, filter, sort=""):
        """
        Extract rows from API
        @param offset: Return results starting with the object at the offset specified
        @param limit: The number of results to display per page.
        @param filter: The result can be filtered by the marked fields in the Fields section below.
                Single filter: &filter=field:value
                Multiple filters: &filter=field:value,field:value
                Date filters: &filter=field:start value|end value (using datetime format)
        @param sort: The result set can be sorted by the marked fields in the Fields section below.
                Format: &sort=field:direction where direction is either asc or desc.
        @return: json data
        """
        field_list = self.column_names_string()
        url = f"{self.url_base}?api_key={self.api_key}&format=json&field_list={field_list}&offset={offset}&limit={limit}&filter={filter}&sort={sort}"
        r = requests_json(url)
        if (r['status_code'] !=1):
            log_ln("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            raise ValueError(r['error'])

        return {'results': r['results'],
                'error': r['error'],
                'number_of_total_results': r['number_of_total_results'],
                'number_of_page_results': r['number_of_page_results']
                }

    # -----------------------------------------------------------------------------
    # TRANSFORM
    # ------------------------------------------------------------------------------
    def etl_transform(self, results, plateform):
        """
        Check field type and transform if needed
        @param results, json rows from extracts
        @return: checked and transformed rows
        """
        rows = []
        for result in results:
            row = {}
            row["id"] = get_int(result["id"])
            row["id_plateform"] = f"{row['id']}_{plateform}"
            # dates
            row["date_added"] = get_datetime(result["date_added"])
            row["original_release_date"] = get_datetime(result["original_release_date"])
            row["date_last_updated"] = get_datetime(result["date_last_updated"])

            # Integers
            row["expected_release_day"] = get_int(result["expected_release_day"])
            row["expected_release_month"] = get_int(result["expected_release_month"])
            row["expected_release_quarter"] = get_int(result["expected_release_quarter"])
            row["expected_release_year"] = get_int(result["expected_release_year"])
            row["number_of_user_reviews"] = get_int(result["number_of_user_reviews"])

            # Strings
            row["aliases"] = get_str(result["aliases"])
            row["api_detail_url"] = get_str(result["api_detail_url"])
            row["deck"] = get_str(result["deck"])
            row["description"] = get_str(result["description"])
            row["guid"] = get_str(result["guid"])
            row["name"] = get_str(result["name"])
            # user want search by title .....
            row["title"] = get_str(result["name"])
            row["site_detail_url"] = get_str(result["site_detail_url"])

            # JSON
            row["image"] = get_json(result["image"])
            # TRANSFORM ARRAY OF JSON IN JSON
            row["image_tags"] = get_json({"image_tags": result["image_tags"]})
            row["original_game_rating"] = get_json({"original_game_rating": result["original_game_rating"]})
            row["platforms"] = get_json({"platforms": result["platforms"]})

            # count how many plateforms concerned this game have and store teh result in __count_platforms__
            # we need that to compare result API and Database due to a game can have more than one platforme
            b = (item for item in result["platforms"] if str(item['id']) in self.plateforms)
            row["__count_platforms__"] = len(list(b))

            row["__rejected_fields_log__"] = "not used yet"

            rows.append(row)
        return rows

    # -----------------------------------------------------------------------------
    # LOAD
    # ------------------------------------------------------------------------------
    def etl_load_bulk(self, session, rows):
        """
        Loading rows by batch in database, used for the init database
        @param session:
        @param rows:
        @return:
        """
        log(f"Loading {len(rows)} row(s)...")
        try:
            session.bulk_insert_mappings(Games, rows)
            log_ln("Done")
            session.commit()
        except exc.SQLAlchemyError as e:
            session.rollback()
            log_ln("Failed")
            log_ln(e)

    def etl_load_one_by_one(self, session, rows):
        """
        Loading rows one by one in database, used for the incremental update database
        @param session:
        @param rows:
        @return:
        """
        for row in rows:
            try:
                # TODO have to find one by one insert  function with mapping !
                session.bulk_insert_mappings(Games, [row])
                session.commit()
            except exc.SQLAlchemyError as e:
                session.rollback()

    # ------------------------------------------------------------------------------
    # Initial loading
    # ------------------------------------------------------------------------------
    def init_load(self, session):
        """
        Load, for each plateforms all the API's rows in the database
        we accept duplicate id, but just after we remove duplicate
        @param session:
        @return:
        """
        for platform_id in self.plateforms:
            r = self.etl_extract(0, 0, f"platforms:{platform_id}", "")
            for offset in range(0, r['number_of_total_results'], self.batch_size):
                # EXTRACT
                r = self.etl_extract(offset, self.batch_size, f"platforms:{platform_id}", "")
                # TRANSFORM
                rows = self.etl_transform(r["results"], platform_id)
                # LOAD
                self.etl_load_bulk(session, rows)
                # suppress duplicate id
        log("Delete duplicate games after BULK import...")
        self.delete_duplicate(session)
        log_ln("done")

    # -----------------------------------------------------------------------------
    # INCREMENTAL UPDATE
    # ------------------------------------------------------------------------------
    def incremental_update_from(self, session, start_date_last_updated):
        """
        Extract,Transform and Load records  update after start_date_last_updated
        @param session:
        @param start_date_last_updated:
        @return:
        """
        for platform_id in self.plateforms:
            # use end date in 2219 !
            s = f"platforms:{platform_id},date_last_updated:{start_date_last_updated}|2219-07-22 02:09:05"
            # find number of rows
            r = self.etl_extract(offset=0, limit=0, filter=s)
            log(f"Incremental update {max(r['number_of_total_results']-1,0)} row(s)...")
            for offset in range(0, r['number_of_total_results'], self.batch_size):
                # EXTRACT
                r = self.etl_extract(offset, self.batch_size, s, "")
                # TRANSFORM
                rows = self.etl_transform(r["results"], platform_id)
                # LOAD
                self.etl_load_one_by_one(session, rows)
            log_ln("Done")
        log("Delete duplicate games after incremental import...")
        self.delete_duplicate(session)
        log_ln("done")

    # -----------------------------------------------------------------------------
    # Update strategy
    # ------------------------------------------------------------------------------
    def incremental_udptate_strategy(self, session):
        '''
        if there is new data in the API compare to the database
            => incremental update
        el if database is more "recent" than the API
            => Reset the database
        else
            => Ok, do nothing

        @param session:
        '''
        log("Check max updated date integrity...")
        max_updated_date_in_api = self.get_date_of_last_updated_row_in_api()
        max_updated_date_in_database = self.get_date_of_last_updated_row_in_database(session)

        if (max_updated_date_in_api > max_updated_date_in_database):
            log_ln(
                "Integrity KO, max_updated_date_in_api>max_updated_date_in_database => STRATEGY TRY INCREMENTAL UPDATE")
            self.incremental_update_from(session, max_updated_date_in_database)
        elif (max_updated_date_in_api < max_updated_date_in_database):
            # RESET DATABASE IF max_updated_date_in_database > max_updated_date_in_api
            # STRATEGY FOR THIS USE CASE : RESET IS BETTER
            log_ln("Integrity KO, database is in advance on API => STRATEGY RESET DATABASE")
            self.reset_table()
            self.init_load(session)
        else:
            log_ln("Integrity OK, database and API have the same max updated date")

    # -----------------------------------------------------------------------------
    # Reset strategy
    # ------------------------------------------------------------------------------
    def reset_database_strategy(self, session):
        '''
        We use this strategie only after an incremental_udptate_strategy
         if we don't have the same number of rows in api and equivalent api number of rows in the database
                = RESET the database
        @param session:
        '''

        log("Check row count integrity..........")
        count_database = self.get_equivalent_api_number_of_row_database(session)
        count_api = self.get_number_of_row_api()

        if count_database != count_api:
            # RESET DATABASE IF COUNT_DATABASE != COUNT_API AFTER an INCREMENTAL UPDATE
            # WE HAVE TO CHECK ONE BY ON => STRATEGY FOR THIS USE CASE : RESET IS BETTER
            log_ln(
                "Integrity NOT OK, count_database != count_api AFTER an INCREMENTAL UPDATE =>STRATEGY RESET DATABASE")
            self.reset_table()
            self.init_load(session)
        else:
            log_ln("Integrity OK, count_database=count_api, same number of rows in API and Database")

    # -----------------------------------------------------------------------------
    # HELPERS TO CHECK INTEGRITY API/DATABASE
    # ------------------------------------------------------------------------------
    def get_number_of_row_api(self):
        '''
        Sum the number of row for each plateforme
        @return: int
        '''
        count = 0
        # we have to request each plateforms
        for platform_id in self.plateforms:
            r = self.etl_extract(offset=0, limit=0, filter=f"platforms:{platform_id}")
            count += r['number_of_total_results']
        return count

    def get_equivalent_api_number_of_row_database(self, session):
        '''
        Due to game has_many plateforms Count line is not enough to check the integrity database,
        # that why we  sum __count_platforms__ to be compare to sum of api results for each plateforme
        @param session:
        @return: int
        '''
        qry = session.query(func.sum(Games.__count_platforms__).label('sum'))
        res = qry.one()
        return res.sum

    def get_date_of_last_updated_row_in_api(self):
        """
        Get the last updated row in the API
        @return: datetime format
        """
        date_of_last_updated_row = []
        for platform_id in self.plateforms:
            r = self.etl_extract(offset=0, limit=1, filter=f"platforms:{platform_id}", sort="date_last_updated:desc")
            date_of_last_updated_row.append(r['results'][0]['date_last_updated'])
        return parse(max(date_of_last_updated_row))

    def get_date_of_last_updated_row_in_database(self, session):
        '''
        Get the last updated row in the database
        @param session:
        @return: datetime
        '''
        qry = session.query(func.max(Games.date_last_updated).label("max_date"))
        res = qry.one()
        return res.max_date

    # -----------------------------------------------------------------------------
    # TABLE HELPERS
    # ------------------------------------------------------------------------------
    def table_exist(self):
        '''
        Test if the table exist
        @return: boolean
        '''
        return self.engine.dialect.has_table(self.engine, self.__tablename__)

    def create_table(self):
        '''
        Create the table in the database
        '''
        self.__table__.create(bind=self.engine, checkfirst=True)

    def drop_table(self):
        '''
        Drop the table in the database
        '''
        self.__table__.drop(bind=self.engine, checkfirst=True)

    def reset_table(self):
        '''
        Reset the table in the databse
        '''
        log("Reset table...")
        self.drop_table()
        self.create_table()
        log_ln("Done")

    def delete_duplicate(self, session):
        '''
       Delete the duplicated ID after  insert on the database
            id1, id1_platform2, ...
            id1, id1_platform3, ...
            id1, id1_platform4, ...
          =>
            id1, id1_platform2, ...
        '''
        session.execute(
            '''     delete from games where id_plateform in (
                    select t.id_plateform FROM
                    (SELECT  id_plateform, RANK() OVER(PARTITION BY id ORDER BY id_plateform) AS rank FROM games) as t 
                    WHERE rank <> 1);''')
        session.commit()

    # -----------------------------------------------------------------------------
    # MODEL HELPERS
    # ------------------------------------------------------------------------------
    def column_names(self):
        """
        Column name of the model
        @return: array of column name
        ex: ['id', 'aliases', 'api_detail_url', 'date_added', ...]
        """
        return [m.key for m in Games.__table__.columns]

    def column_names_string(self):
        """
        Column name of the model
        @return: string of column names seprate by ,
        ex: "id,aliases,api_detail_url,date_added,date_last_updated,deck,..."
        """
        return ",".join(self.column_names())
