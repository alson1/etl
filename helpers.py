import datetime,time
from dateutil.parser import parse
import os
import pickle
import hashlib
import json
from config import config
import re
import requests

#------------------------------------------------------------
# HELPERS FOR EXTRACT
#-----------------------------------------------------------
def get_int(value, default=None):
    try:
        return int(value)
    except Exception:
        return default


def get_str(value, default=None):
    try:
        return str(value)
    except Exception:
        return default


def get_datetime(value, default=None):
    try:
        return parse(value)
    except Exception:
        return default


def get_json(value, default=None):
    try:
        if type(value) is dict:
            js = json.loads(json.dumps(value))
            return value
        else:
            return default
    except Exception:
        return default


#------------------------------------------------------------
# HELPERS FOR CACHED REQUEST
#-----------------------------------------------------------
def cached():
    """
    A function that creates a decorator which will use config.cache_dir for caching the results of the decorated function "fn".
    """

    def decorator(fn):  # define a decorator for a function "fn"
        def wrapped(*args, **kwargs):  # define a wrapper that will finally call "fn" with all arguments
            cachedir = config.cache_dir
            cond = config.cache
            if not cond:
                return fn(*args, **kwargs)
            # we remove API_KEY from url for cache, then we will not need API_KEY to reuse cached request in TESTS
            url_with_out_api_key = re.sub(r'api_key=.+&format=', '', args[0])

            filename = cachedir + "/" + hashlib.md5(url_with_out_api_key.encode("utf")).hexdigest() + ".pickle"
            # if cache exists -> load it and return its content
            if os.path.exists(filename):
                with open(filename, 'rb') as cachehandle:
                    log_ln("using cached result from '%s'" % cachedir + filename)
                    return pickle.load(cachehandle)

            # execute the function with all arguments passed
            res = fn(*args, **kwargs)

            # write to cache file
            with open(filename, 'wb') as cachehandle:
                log_ln("saving result to cache '%s'" % filename)
                pickle.dump(res, cachehandle)

            return res

        return wrapped

    return decorator  # return this "customized" decorator that uses "cachefile"

#------------------------------------------------------------
# HELPERS FOR HTTP REQUEST
#-----------------------------------------------------------

# @cached(config.cache_dir,config.cache)
# not WORKING get the first time value in config.py
# WORKAROUND : put the parametre inside the decorator
@cached()
def requests_json(url):
    headers = {'User-Agent': 'Mozilla/5.0'}
    #In case of big network problem we don't want a perpetual waitting
    # may be optimized with requests.adapters for retry management!
    try:
        r = requests.get(url, headers=headers,timeout=config.timeout)
        return r.json()
    except Exception as x:
        log_ln(f'Error 1/3 : {x}')

    try:
        log_ln(f"Wait {config.sleeping}s....")
        time.sleep(config.sleeping)
        r = requests.get(url, headers=headers,timeout=config.timeout)
        return r.json()
    except Exception as x:
        log_ln(f'Error 2/3 : {x}')

    try:
        log_ln(f"Wait {config.sleeping}s....")
        time.sleep(config.sleeping)
        r = requests.get(url, headers=headers,timeout=config.timeout)
        return r.json()
    except Exception as x:
        log_ln(f'Error 3/3 : {x}')
        raise

#------------------------------------------------------------
# HELPERS FOR LOGGING
#-----------------------------------------------------------
def log(s):
    print(f"[{os.environ['HOSTNAME']}] {datetime.datetime.now()} {s}", end='', flush=True)


def log_ln(s):
    print(s, flush=True)
