import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from games import Games
from config import settings
from helpers import log_ln,get_int,get_str
#------------------------------------------------
# FOR DEV ENVIRONMENT
# PUT variables in your os variable environment :
# env='dev'
# api_key="XXXXX_your_api_key"
# batch_size =100
#------------------------------------------------
def main():
    log_ln("----------------------------")
    log_ln("Data Engineer Take Home Test")
    log_ln("ETL GiantBomb API => SQLite")
    log_ln("----------------------------")

    try:
        if os.environ['env']=="dev":
            settings.init_dev()
        else:
            settings.init_prod()
    except:
        #prod env by default if env not defined
        settings.init_prod()

    try:
        a=os.environ['batch_size']
    except:
        log_ln("Missing batch_size")
        exit(1)
    try:
        a=os.environ['api_key']
    except:
        log_ln("Missing api_key")
        exit(1)

    # Due to docker integration we use OS environment variables to share the cmd line parameters
    api_key = get_str(os.environ['api_key'])
    if (api_key == None):
        log_ln("Missing api_key")
        exit(1)
    # batch_size limit to 100 cf http://www.giantbomb.com/api/documentation#toc-0-15
    batch_size = get_int(os.environ['batch_size'])
    if (batch_size==None or batch_size>100 or batch_size <1):
        log_ln("Missing or bad batch_size must be between 1 and 100")
        exit(1)
    # ----------------------------------------------
    games = Games(create_engine('sqlite:///db/games.db', echo=False), api_key, batch_size)
    Session = sessionmaker(bind=games.engine)
    Session.configure(bind=games.engine)
    session = Session()

    log_ln("----------------------------")
    log_ln("Check IF table exists")
    log_ln("----------------------------")
    if not games.table_exist():
        log_ln("Database doesn't exists")
        try:
            games.reset_table()
            games.init_load(session)
        except Exception as x:
            log_ln(f'Error : {x}')
            log_ln(f'Database may be not fully synchronized')

    else:
        log_ln('OK')

    log_ln("------------------------------------")
    log_ln("Check IF incremental update is needed")
    log_ln("------------------------------------")
    try:
        games.incremental_udptate_strategy(session)
    except Exception as x:
        log_ln(f'Error : {x}')
        log_ln(f'Database may be not fully synchronized')
        log_ln(f'Try to relaunch the app')


    log_ln("-----------------------------------")
    log_ln("Check IF reset database is needed")
    log_ln("-----------------------------------")
    try:
        games.reset_database_strategy(session)
    except Exception as x:
        log_ln(f'Error : {x}')
        log_ln(f'Database may be not fully synchronized')
        log_ln(f'Try to relaunch the app')

    session.close()

if __name__ == '__main__':
    main()